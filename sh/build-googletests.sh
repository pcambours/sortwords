#!/bin/sh

GOOGLETESTS_DIR=../vendor/google/googletest

(rm -rf build &>/dev/null; mkdir build; cd build; cmake $GOOGLETESTS_DIR -DCMAKE_CXX_FLAGS=--std=c++11 -DCMAKE_INSTALL_PREFIX=..; make; make install; cd ..; rm -rf build > /dev/null)
