#pragma once

#include <QStringList>

namespace samplelibrary
{

    QStringList sortWords(const QStringList& wordList, bool unique, bool reverseSort);

}

