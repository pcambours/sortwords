#include "sortwordslib.h"

#include <QMap>
#include <QMultiMap>
#include <QMapIterator>
#include <QString>
#include <QScopedPointer>

namespace samplelibrary
{

    QStringList sortWords(const QStringList& wordList, bool unique, bool reverseSort)
    {
        QScopedPointer<QMap<QString, QString>> mapPtr;

        if (unique)
        {
            auto uniqueMap = new QMap<QString, QString>;
            for (const auto& word: wordList)
            {
                uniqueMap->insert(word.toLower(), word);
            }
            mapPtr.reset(uniqueMap);
        }
        else
        {
            auto multiMap = new QMultiMap<QString, QString>;
            for (const auto& word: wordList)
            {
                multiMap->insert(word.toLower(), word);
            }
            mapPtr.reset(multiMap);
        }

        QStringList result;
        QMapIterator<QString,QString> iterator(*mapPtr);
        if (reverseSort)
        {
            iterator.toBack();
            while (iterator.hasPrevious())
            {
                iterator.previous();
                result.append(iterator.value());
            }
        }
        else
        {
            iterator.toFront();
            while (iterator.hasNext())
            {
                iterator.next();
                result.append(iterator.value());
            }
        }
        return result;
    }

}


