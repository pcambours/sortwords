#include "sortwordslib.h"

#include <QCoreApplication>
#include <QCommandLineParser>
#include <QDebug>
#include <QString>

using namespace samplelibrary;

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    QCoreApplication::setApplicationName("SortWords");
    QCoreApplication::setApplicationVersion("1.0");

    QCommandLineParser parser;
    parser.setApplicationDescription("SortWords");
    parser.addHelpOption();
    parser.addVersionOption();

    QCommandLineOption uniqueWordsOption(QStringList() << "u" << "unique",
        QCoreApplication::translate("main", "all words are unique in the result list"));
    parser.addOption(uniqueWordsOption);

    QCommandLineOption reverseSortOption(QStringList() << "r" << "reverse",
            QCoreApplication::translate("main", "words are sorted in the reverse order."));
    parser.addOption(reverseSortOption);

    parser.process(app);

    auto uniqueWords = false;
    if (parser.isSet("u"))
    {
        uniqueWords = true;
    }
    auto reverSort = false;
    if (parser.isSet("r"))
    {
        reverSort = true;
    }
    // removing options from the command line
    auto commandLine = QCoreApplication::arguments();
    for (int i = 0 ; i < parser.optionNames().length() + 1 ; ++i)
    {
        commandLine.removeFirst();
    }

    // proceed the sort
    auto result = sortWords(commandLine, uniqueWords, reverSort);

    // displaying the result
    for (const auto& element: result)
    {
        qDebug() << element;
    }

}
