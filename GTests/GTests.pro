TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG += qt

SOURCES += \
        SampleLibraryTest.cpp \
        main.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../SortWordsLib/release/ -lsortwordslib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../SortWordsLib/debug/ -lsortwordslib
else:unix: LIBS += -L$$OUT_PWD/../SortWordsLib/ -lsortwordslib

INCLUDEPATH += $$PWD/../SortWordsLib
DEPENDPATH += $$PWD/../SortWordsLib

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../GoogleTests/lib/release/ -lgtest
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../GoogleTests/lib/debug/ -lgtest
else:unix: LIBS += -L$$PWD/../GoogleTests/lib/ -lgtest

INCLUDEPATH += $$PWD/../GoogleTests/include
DEPENDPATH += $$PWD/../GoogleTests/include

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../GoogleTests/lib/release/libgtest.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../GoogleTests/lib/debug/libgtest.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../GoogleTests/lib/release/gtest.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../GoogleTests/lib/debug/gtest.lib
else:unix: PRE_TARGETDEPS += $$PWD/../GoogleTests/lib/libgtest.a
