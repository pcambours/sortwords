#include "gtest/gtest.h"

#include "sortwordslib.h"


TEST(SortTest, emptyList)
{
    QStringList entryList;
    QStringList outputList;
    ASSERT_EQ(samplelibrary::sortWords(entryList, false, false).size(), 0);
}

TEST(SortTest, oneElementList)
{
    QStringList entryList { "chaton" };
    QStringList outputList { "chaton" };
    ASSERT_EQ(samplelibrary::sortWords(entryList, false, false), outputList);
}

TEST(SortTest, multipleElementList)
{
    QStringList entryList { "lynx", "chaton", "tigre", "panthere", "Lynx"  };
    QStringList outputList { "chaton", "Lynx", "lynx", "panthere", "tigre" };
    ASSERT_EQ(samplelibrary::sortWords(entryList, false, false), outputList);
}

TEST(SortTest, uniqueElementList)
{
    QStringList entryList { "lynx", "chaton", "tigre", "panthere", "Lynx"  };
    QStringList outputList { "chaton", "Lynx", "panthere", "tigre" };
    ASSERT_EQ(samplelibrary::sortWords(entryList, true, false), outputList);
}
